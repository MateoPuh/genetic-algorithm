package hr.fer.zemris.evolutionary.model

import hr.fer.zemris.evolutionary.dataset.Dataset
import hr.fer.zemris.evolutionary.algorithm.generateRandomValue
import kotlin.math.pow

data class EvolutionaryUnit(
    val b0: Double,
    val b1: Double,
    val b2: Double,
    val b3: Double,
    val b4: Double
) {

    companion object {

        fun random() = EvolutionaryUnit(
            generateRandomValue(),
            generateRandomValue(),
            generateRandomValue(),
            generateRandomValue(),
            generateRandomValue()
        )
    }

    fun copy(b0: Double?, b1: Double?, b2: Double?, b3: Double?, b4: Double?) = EvolutionaryUnit(
        b0 ?: this.b0,
        b1 ?: this.b1,
        b2 ?: this.b2,
        b3 ?: this.b3,
        b4 ?: this.b4
    )

    fun meanSquaredError(dataset: Dataset, characteristic: Characteristic) =
        dataset.dataset.entries
            .map { (input, value) ->
                (value - characteristic.calculate(input.first, input.second, this)).pow(2)
            }
            .reduce(Double::plus).div(dataset.dataset.size)

    override fun toString(): String {
        val stringBuilder = StringBuilder()

        stringBuilder.append("[")
        stringBuilder.append(b0.toString())
        stringBuilder.append(" ")
        stringBuilder.append(b1.toString())
        stringBuilder.append(" ")
        stringBuilder.append(b2.toString())
        stringBuilder.append(" ")
        stringBuilder.append(b3.toString())
        stringBuilder.append(" ")
        stringBuilder.append(b4.toString())
        stringBuilder.append("]")

        return stringBuilder.toString()
    }
}
