package hr.fer.zemris.evolutionary.model

import kotlin.math.cos
import kotlin.math.exp
import kotlin.math.pow
import kotlin.math.sin

class CharacteristicImpl: Characteristic {

    override fun calculate(x: Double, y: Double, unit: EvolutionaryUnit) = with(unit) {
        sin(b0 + b1 * x) + b2 * cos(x * (b3 + y)) * (1.0 / (1.0 + exp((x - b4).pow(2.0))))
    }
}
