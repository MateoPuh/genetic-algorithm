package hr.fer.zemris.evolutionary.model

import hr.fer.zemris.evolutionary.operators.Fitness
import hr.fer.zemris.evolutionary.algorithm.randomProbability
import hr.fer.zemris.evolutionary.dataset.Dataset
import kotlin.random.Random

class Population(
    private val units: MutableList<EvolutionaryUnit>
) {

    private var maxError: Double? = null

    companion object {

        fun generateRandom(size: Int) = Population(MutableList(size) { EvolutionaryUnit.random() })
    }

    fun size() = units.size

    fun add(unit: EvolutionaryUnit) {
        maxError = null
        units.add(unit)
    }

    fun remove(unit: EvolutionaryUnit) {
        maxError = null
        units.remove(unit)
    }

    fun getMaxError(dataset: Dataset, characteristic: Characteristic): Double {
        if (maxError == null) {
            maxError = units.map { it.meanSquaredError(dataset, characteristic) }.max()!!
        }

        return maxError!!
    }

    fun getRandomUnit() = Random.nextInt(0, units.size - 1).let(units::get)

    fun getBestUnit(fitness: Fitness): EvolutionaryUnit = units.maxBy(fitness::getFitness)!!

    fun getRandomProportionalUnits(fitness: Fitness, k: Int): List<EvolutionaryUnit> {
        val total = totalFitness(fitness)
        return (0..k).map { randomProbability() }.map { getUnitForProbability(it, fitness, total) }
    }

    private fun getUnitForProbability(prob: Double, fitness: Fitness, total: Double): EvolutionaryUnit {
        var acc = 0.0
        var resultIndex = 0

        units.map(fitness::getFitness)
            .map { it / total }
            .forEachIndexed { index, d ->
                acc += d
                if (prob < acc) {
                    resultIndex = index
                }
            }

        return units[resultIndex]
    }

    private fun totalFitness(fitness: Fitness) = units.map(fitness::getFitness).reduce(Double::plus)
}
