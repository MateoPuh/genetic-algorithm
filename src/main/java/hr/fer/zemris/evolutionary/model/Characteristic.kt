package hr.fer.zemris.evolutionary.model

interface Characteristic {

    fun calculate(x: Double, y: Double, unit: EvolutionaryUnit): Double
}
