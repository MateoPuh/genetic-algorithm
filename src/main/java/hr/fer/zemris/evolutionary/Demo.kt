package hr.fer.zemris.evolutionary

import hr.fer.zemris.evolutionary.algorithm.AlgorithmConfiguration
import hr.fer.zemris.evolutionary.algorithm.EliminationalAlgorithm
import hr.fer.zemris.evolutionary.algorithm.GenerationalAlgorithm
import hr.fer.zemris.evolutionary.dataset.Dataset
import hr.fer.zemris.evolutionary.model.CharacteristicImpl
import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import hr.fer.zemris.evolutionary.model.Population
import hr.fer.zemris.evolutionary.operators.*

private const val ELIMINATIONAL_MAX_ITERATIONS = 20000
private const val GENERATIONAL_MAX_ITERATIONS = 1000

private const val ELIMINATIONAL_MUTATION_PROBABILITY = 0.8
private const val GENERATIONAL_MUTATION_PROBABILITY = 0.7

private const val ELIMINATIONAL_POPULATION_SIZE = 150
private const val GENERATIONAL_POPULATION_SIZE = 80

fun main() {
    val configuration = generationalConfiguration()
    val dataset = dataset1()
    val characteristic = CharacteristicImpl()

    val mutation = SimpleMutation(configuration.mutationProbability)
    val recombination = DiscreteRecombination()
    val selection = ProportionalSelection()

    fun printBestUnit(unit: EvolutionaryUnit, generationNumber: Int) {
        println("Unit " + unit.toString() + ", generation: " + generationNumber + ", error: " + unit.meanSquaredError(dataset, characteristic))
    }

    val population = Population.generateRandom(configuration.populationSize)
    val fitness = FitnessImpl(dataset, characteristic, population)

    val algorithm = GenerationalAlgorithm(
        population = population,
        configuration = configuration,
        mutation = mutation,
        recombination = recombination,
        selection = selection,
        bestUnitListener = ::printBestUnit,
        fitness = fitness,
        elitism = true
    )

    algorithm.run()

    println("KRAJ")
}

private fun eliminationalConfiguration() = AlgorithmConfiguration(
    populationSize = ELIMINATIONAL_POPULATION_SIZE,
    mutationProbability = ELIMINATIONAL_MUTATION_PROBABILITY,
    maxIterations = ELIMINATIONAL_MAX_ITERATIONS
)

private fun generationalConfiguration() = AlgorithmConfiguration(
    populationSize = GENERATIONAL_POPULATION_SIZE,
    mutationProbability = GENERATIONAL_MUTATION_PROBABILITY,
    maxIterations = GENERATIONAL_MAX_ITERATIONS
)

private fun dataset1() = Dataset("src/dataset1.txt")

private fun dataset2() = Dataset("src/dataset2.txt")
