package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit

interface Fitness {

    fun getFitness(unit: EvolutionaryUnit): Double
}
