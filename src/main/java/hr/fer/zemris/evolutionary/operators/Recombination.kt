package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import kotlin.random.Random

interface Recombination {

    fun recombine(unit1: EvolutionaryUnit, unit2: EvolutionaryUnit): EvolutionaryUnit
}
