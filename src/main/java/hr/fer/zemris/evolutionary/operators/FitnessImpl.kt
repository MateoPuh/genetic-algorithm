package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.dataset.Dataset
import hr.fer.zemris.evolutionary.model.Characteristic
import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import hr.fer.zemris.evolutionary.model.Population

class FitnessImpl(
    private val dataset: Dataset,
    private val characteristic: Characteristic,
    private val population: Population
) : Fitness {

    override fun getFitness(unit: EvolutionaryUnit) = population.getMaxError(dataset, characteristic) - unit.meanSquaredError(dataset, characteristic)
}
