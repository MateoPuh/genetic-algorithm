package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.model.Population

class RandomSelection : Selection {

    override fun select(population: Population, k: Int, fitness: Fitness) = (0..k).map { population.getRandomUnit() }
}
