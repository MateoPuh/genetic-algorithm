package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.algorithm.generateRandomValue
import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import kotlin.random.Random

class SimpleMutation(
    private val mutationProbability: Double
) : Mutation {

    override fun mutate(unit: EvolutionaryUnit) = if (shouldMutateRandom()) { getMutatedUnit(unit) } else { unit }

    private fun getMutatedUnit(unit: EvolutionaryUnit) = unit.copy(
        getMutatedValue(),
        getMutatedValue(),
        getMutatedValue(),
        getMutatedValue(),
        getMutatedValue()
    )

    private fun getMutatedValue() = if (shouldMutateRandom()) generateRandomValue() else null

    private fun shouldMutateRandom() = randomDouble() < mutationProbability

    private fun randomDouble() = Random.nextDouble(0.0, 1.0)
}
