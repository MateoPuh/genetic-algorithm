package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.algorithm.randomProbability
import hr.fer.zemris.evolutionary.model.EvolutionaryUnit

private const val DISCRETE_RECOMBINATION_PROBABILITY = 0.5

class DiscreteRecombination : Recombination {

    override fun recombine(unit1: EvolutionaryUnit, unit2: EvolutionaryUnit) = unit1.copy(
        getValue(unit1.b0, unit2.b0),
        getValue(unit1.b1, unit2.b1),
        getValue(unit1.b2, unit2.b2),
        getValue(unit1.b3, unit2.b3),
        getValue(unit1.b4, unit2.b4)
    )

    private fun getValue(unit1b: Double, unit2b: Double) = if (shouldTakeFirst()) unit1b else unit2b

    private fun shouldTakeFirst() = randomProbability() < DISCRETE_RECOMBINATION_PROBABILITY
}
