package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit

interface Mutation {

    fun mutate(unit: EvolutionaryUnit): EvolutionaryUnit
}
