package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import hr.fer.zemris.evolutionary.model.Population

interface Selection {

    fun select(population: Population, k: Int, fitness: Fitness): List<EvolutionaryUnit>
}
