package hr.fer.zemris.evolutionary.operators

import hr.fer.zemris.evolutionary.model.Population

class ProportionalSelection : Selection {

    override fun select(population: Population, k: Int, fitness: Fitness) = population.getRandomProportionalUnits(fitness, k)
}
