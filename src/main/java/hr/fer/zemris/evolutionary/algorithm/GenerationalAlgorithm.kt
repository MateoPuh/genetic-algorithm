package hr.fer.zemris.evolutionary.algorithm

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import hr.fer.zemris.evolutionary.model.Population
import hr.fer.zemris.evolutionary.operators.Fitness
import hr.fer.zemris.evolutionary.operators.Mutation
import hr.fer.zemris.evolutionary.operators.Recombination
import hr.fer.zemris.evolutionary.operators.Selection

class GenerationalAlgorithm(
    private var population: Population,
    private val configuration: AlgorithmConfiguration,
    private val mutation: Mutation,
    private val recombination: Recombination,
    private val selection: Selection,
    private val bestUnitListener: BestUnitListener,
    private val fitness: Fitness,
    private val elitism: Boolean
) {

    private var bestUnit = population.getBestUnit(fitness)
    private var generationNumber = 0

    fun run() {
        for (i in 0..configuration.maxIterations) {
            generationNumber++

            val newUnits = mutableListOf<EvolutionaryUnit>()

            if (elitism) {
                newUnits.add(population.getBestUnit(fitness))
            }

            for (u in 0..(population.size() - if (elitism) 1 else 0)) {
                val parents = selection.select(population, 2, fitness)
                val child = recombination.recombine(parents[0], parents[1])
                val mutated = mutation.mutate(child)
                newUnits.add(mutated)
            }

            population = Population(newUnits)

            val newBest = population.getBestUnit(fitness)

            if (fitness.getFitness(newBest) > fitness.getFitness(bestUnit)) {
                bestUnit = newBest
                bestUnitListener(bestUnit, generationNumber)
            }
        }
    }
}
