package hr.fer.zemris.evolutionary.algorithm

data class AlgorithmConfiguration(
    val populationSize: Int,
    val mutationProbability: Double,
    val maxIterations: Int
)
