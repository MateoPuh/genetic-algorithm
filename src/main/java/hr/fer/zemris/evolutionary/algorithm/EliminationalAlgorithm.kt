package hr.fer.zemris.evolutionary.algorithm

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import hr.fer.zemris.evolutionary.model.Population
import hr.fer.zemris.evolutionary.operators.Fitness
import hr.fer.zemris.evolutionary.operators.Mutation
import hr.fer.zemris.evolutionary.operators.Recombination
import hr.fer.zemris.evolutionary.operators.Selection

class EliminationalAlgorithm(
    private var population: Population,
    private val configuration: AlgorithmConfiguration,
    private val mutation: Mutation,
    private val recombination: Recombination,
    private val selection: Selection,
    private val bestUnitListener: BestUnitListener,
    private val fitness: Fitness
) {

    private var bestUnit = population.getBestUnit(fitness)
    private var generationNumber = 0

    fun run() {
        for (i in 0..configuration.maxIterations) {
            generationNumber++

            val selection = selection.select(population, 3, fitness)
            val parents = getParents(selection)
            val child = recombination.recombine(parents[0], parents[1])
            val mutated = mutation.mutate(child)
            updatePopulation(selection, mutated)

            val newBest = population.getBestUnit(fitness)

            if (fitness.getFitness(newBest) > fitness.getFitness(bestUnit)) {
                bestUnit = newBest
                bestUnitListener(bestUnit, generationNumber)
            }
        }
    }

    private fun getParents(selection: List<EvolutionaryUnit>) = selection.sortedByDescending(fitness::getFitness).take(2)

    private fun updatePopulation(selection: List<EvolutionaryUnit>, mutatedChild: EvolutionaryUnit) {
        population.remove(selection.minBy(fitness::getFitness)!!)
        population.add(mutatedChild)
    }
}