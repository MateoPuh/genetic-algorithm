package hr.fer.zemris.evolutionary.algorithm

import hr.fer.zemris.evolutionary.model.EvolutionaryUnit
import kotlin.random.Random

typealias BestUnitListener = (EvolutionaryUnit, Int) -> Unit

private const val BOTTOM_LIMIT = -4.0
private const val TOP_LIMIT = 4.0

fun generateRandomValue() = Random.nextDouble(BOTTOM_LIMIT, TOP_LIMIT)

fun randomProbability() = Random.nextDouble(0.0, 1.0)
