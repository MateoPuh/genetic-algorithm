package hr.fer.zemris.evolutionary.dataset

import java.io.File

const val DATA_DELIMITER = "\t"

typealias Input = Pair<Double, Double>

class Dataset(fileName: String) {

    val dataset: Map<Input, Double> = File(fileName).useLines { it.toMap() }
}

private fun Sequence<String>.toMap() = map(String::toMapEntry).toMap()

private fun String.toMapEntry() = split(DATA_DELIMITER).let {
    Input(it[0].toDouble(), it[1].toDouble()) to it[2].toDouble()
}
